package org.yah.tools.cssax;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonToken;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.junit.Ignore;
import org.yah.tools.cssax.selector.SelectorLexer;
import org.yah.tools.cssax.selector.SelectorParser;
import org.yah.tools.cssax.selector.SelectorParser.selectors_return;

@Ignore
public class SelectorParserTest {

	private static final String LINE_FEED = System.getProperty("line.separator");

	public static void main(String[] args) throws RecognitionException {
//		String input = "div:nth-child( 4n+1 )";
		String input = "div:not(.test)";
		SelectorLexer lexer = new SelectorLexer(new ANTLRStringStream(input));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		tokenStream.fill();
		System.out.println(tokenStream.getTokens());
		SelectorParser parser = new SelectorParser(tokenStream);
		selectors_return selectors = parser.selectors();
		CommonTree tree = (CommonTree) selectors.getTree();
		System.out.println(printTree(tree, SelectorParser.tokenNames));
	}

	public static String printTree(Tree tree, String[] tokenNames) {
		StringBuilder sb = new StringBuilder();
		printTree(tree, sb, 0, tokenNames);
		return sb.toString();
	}

	private static void printTree(Tree tree, StringBuilder sb, int level, String[] tokenNames) {
		if (tree == null)
			return;

		for (int i = 0; i < level; i++)
			sb.append("  ");

		sb.append(tree.toString());

		int type = tree.getType();
		if (type > 0 && type < tokenNames.length)
			sb.append(" (").append(tokenNames[type]).append(")");
		else
			sb.append(" (").append(type).append(")");

		if (tree instanceof CommonTree && ((CommonTree) tree).token != null) {
			CommonToken token = (CommonToken) ((CommonTree) tree).token;
			sb.append(" [").append(token.getStartIndex()).append(',').append(token.getStopIndex()).append(']');
		}

		sb.append(LINE_FEED);
		for (int i = 0; i < tree.getChildCount(); i++) {
			Tree child = tree.getChild(i);
			printTree(child, sb, level + 1, tokenNames);
		}
	}

}
