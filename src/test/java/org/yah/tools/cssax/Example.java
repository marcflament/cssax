/**
 * 
 */
package org.yah.tools.cssax;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.yah.tools.cssax.selector.Element;
import org.yah.tools.cssax.selector.SaxHtmlExtractor;
import org.yah.tools.cssax.selector.HtmlParsingException;
import org.yah.tools.cssax.selector.SelectorBinding;
import org.yah.tools.cssax.selector.Selectors;
import org.yah.tools.cssax.selector.annotations.AnnotationSelectorBindingsFactory;
import org.yah.tools.cssax.selector.annotations.Selector;

/**
 * @author Yah
 *
 */
public class Example {

	/**
	 * Method invocation order is first controlled by CSS specificity order or
	 * by method declaration order for same specificity.
	 */
	public static class ExampleExtractor {
		@Selector(".selected")
		public void extractByClass(Element e) {
			System.out.println("extractByClass : " + e);
		}

		// will be called before .selected selector since # is more specific
		@Selector("#second")
		public void extractById(Element e) {
			System.out.println("extractById : " + e);
		}

		@Selector("li")
		public void extractAllLi(Element e) {
			System.out.println("extractAllLi : " + e);
		}

		@Selector("ul")
		public void extractInner(Element e) {
			Element li = e.findFirst(new Selectors("#first"));
			System.out.println("extractInner : " + li + " extracted from " + e);
		}
		
	}

	public static void main(String[] args) throws Exception {
		// create the HTML extractor
		SaxHtmlExtractor htmlExtractor = new SaxHtmlExtractor(Charset.forName("UTF-8"));

		// extending a single selector binding
		InputStream htmlStream = Example.class.getResourceAsStream("/example.html");
		try {
			// you can also pass a SelectorListener instead of extending the
			// SelectorBinding
			SelectorBinding binding = new SelectorBinding("#paragraph1") {
				@Override
				public void elementMatched(Element element) throws HtmlParsingException {
					System.out.println("Paragraph 1 content : " + element.getText());
				}
			};
			htmlExtractor.parseHtml(htmlStream, binding);
		} finally {
			IOUtils.closeQuietly(htmlStream);
		}

		// using annotations
		htmlStream = Example.class.getResourceAsStream("/example.html");
		try {
			AnnotationSelectorBindingsFactory factory = new AnnotationSelectorBindingsFactory();
			// you can filter methods of the extractor by method names
			SelectorBinding[] bindings = factory.createBindings(new ExampleExtractor());
			htmlExtractor.parseHtml(htmlStream, bindings);
		} finally {
			IOUtils.closeQuietly(htmlStream);
		}
	}
}
