package org.yah.tools.cssax.selector;

import static junit.framework.Assert.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.yah.tools.cssax.selector.Element;
import org.yah.tools.cssax.selector.ElementHandlerException;
import org.yah.tools.cssax.selector.SaxHtmlExtractor;
import org.yah.tools.cssax.selector.HtmlParsingException;
import org.yah.tools.cssax.selector.SelectorBinding;
import org.yah.tools.cssax.selector.SelectorBindingsBuilder;
import org.yah.tools.cssax.selector.SelectorParsingException;
import org.yah.tools.cssax.selector.Selectors;
import org.yah.tools.cssax.selector.annotations.AnnotationSelectorBindingsFactory;
import org.yah.tools.cssax.selector.annotations.Selector;

public class HtmlExtractorTest {

	private SaxHtmlExtractor extractor;

	@Before
	public void setup() throws Exception {
		extractor = new SaxHtmlExtractor("UTF-8");
	}

	private void parse(String file, SelectorBinding... bindings) throws Exception {
		InputStream htmlStream = getClass().getResourceAsStream(file);
		try {
			extractor.parseHtml(htmlStream, bindings);
		} finally {
			IOUtils.closeQuietly(htmlStream);
		}
	}

	@Test
	public void testHtmlParsingException() throws Exception {
		SelectorBinding binding = new SelectorBinding("#div1") {
			@Override
			public void elementMatched(Element element) throws ElementHandlerException {
				throw new ElementHandlerException("test error");
			}
		};
		try {
			parse("/test-predicates.html", binding);
			fail("Should have failed");
		} catch (HtmlParsingException e) {
			// expected
		}
	}

	@Test
	public void testPredicates() throws Exception {
		ExpectedsMap expecteds = new ExpectedsMap();
		expecteds.addExpectded("#div1", "html[0] > body[0] > div[0]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p[dir]", "html[0] > body[0] > p[0]", "html[0] > body[0] > p[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p[dir=rtl]", "html[0] > body[0] > p[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div.nested", "html[0] > body[0] > div[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div:not(.nested)", "html[0] > body[0] > div[0]", "html[0] > body[0] > div[2]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p:not([dir])", "html[0] > body[0] > p[2]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div:first-child", "html[0] > body[0] > div[0]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p:first-child");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p:first-of-type", "html[0] > body[0] > p[0]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div:nth-child(4)", "html[0] > body[0] > div[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div:nth-child(2)");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("*:nth-child(2)", "html[0] > body[0] > p[0]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("p:nth-child(3)", "html[0] > body[0] > p[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("div:nth-of-type(2)", "html[0] > body[0] > div[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();

		expecteds = new ExpectedsMap();
		expecteds.addExpectded("*:nth-of-type(2)", "html[0] > body[0] > p[1]", "html[0] > body[0] > div[1]");
		parse("/test-predicates.html", expecteds.createBindings());
		expecteds.validate();
	}

	@Test
	public void testDescendant() throws Exception {
		ExpectedsMap expecteds = new ExpectedsMap();
		expecteds.addExpectded("body div", "html[0] > body[0] > div[0]", "html[0] > body[0] > div[0] > div[0]",
				"html[0] > body[0] > div[0] > div[0] > div[0]");
		parse("/test-descendant-child.html", expecteds.createBindings());
		expecteds.validate();
	}

	@Test
	public void testChild() throws Exception {
		ExpectedsMap expecteds = new ExpectedsMap();
		expecteds.addExpectded("body > div", "html[0] > body[0] > div[0]");
		parse("/test-descendant-child.html", expecteds.createBindings());
		expecteds.validate();
	}

	@Test
	public void testAdjacentSibling() throws Exception {
		ExpectedsMap expecteds = new ExpectedsMap();
		expecteds.addExpectded("h2+p", "html[0] > body[0] > p[1]", "html[0] > body[0] > p[4]");
		parse("/test-sibling.html", expecteds.createBindings());
		expecteds.validate();
	}

	@Test
	public void testGeneralSibling() throws Exception {
		ExpectedsMap expecteds = new ExpectedsMap();
		expecteds.addExpectded("h2~p", "html[0] > body[0] > p[1]", "html[0] > body[0] > p[2]",
				"html[0] > body[0] > p[3]", "html[0] > body[0] > p[4]");
		parse("/test-sibling.html", expecteds.createBindings());
		expecteds.validate();
	}

	public static class SpecificityValidator {
		boolean matched;

		@Selector(".bar")
		public void matchClass() {
			assertTrue(matched);
		}

		@Selector("#foo")
		public void matchId() {
			assertFalse(matched);
			matched = true;
		}

	}

	@Test
	public void testSpecificity() throws Exception {
		AnnotationSelectorBindingsFactory factory = new AnnotationSelectorBindingsFactory();
		parse("/test-specificity.html", factory.createBindings(new SpecificityValidator()));
	}

	@Test
	public void testBug() throws Exception {
		parse("/test-bug.html", new SelectorBinding("div#col_main section.media-meta-list span.title a") {
			@Override
			public void elementMatched(Element element) throws HtmlParsingException {
				System.out.println(element.getFullText());
			}
		});
	}

	private class ExpectedsMap extends HashMap<String, List<String>> {
		private static final long serialVersionUID = 1L;

		private List<ValidatorHandler> handlers;

		public ExpectedsMap() {
			super();
		}

		public void addExpectded(String selector, String... nodes) {
			List<String> list = get(selector);
			if (list == null)
				put(selector, list = new LinkedList<String>());
			for (int i = 0; i < nodes.length; i++)
				list.add(nodes[i]);
		}

		public SelectorBinding[] createBindings() throws SelectorParsingException {
			handlers = new ArrayList<ValidatorHandler>(size());
			SelectorBindingsBuilder builder = new SelectorBindingsBuilder();
			for (Map.Entry<String, List<String>> entry : entrySet()) {
				Selectors selectors = new Selectors(entry.getKey());
				ValidatorHandler handler = new ValidatorHandler(selectors, entry.getValue());
				handlers.add(handler);
				builder.addBinding(handler);
			}
			return builder.build();
		}

		public void validate() {
			for (ValidatorHandler handler : handlers) {
				handler.validate();
			}
		}
	}

	private class ValidatorHandler extends SelectorBinding {

		private final List<String> expectedElements;

		public ValidatorHandler(Selectors selectors, List<String> expectedElements) {
			super(selectors);
			this.expectedElements = expectedElements;
		}

		@Override
		public void elementMatched(Element element) throws HtmlParsingException {
			if (expectedElements.isEmpty())
				fail(getSelectors() + " : Unexpected element " + element);

			String expected = expectedElements.remove(0);

			if (!expected.equals(element.toString()))
				fail(getSelectors() + " : Unexpected element " + element + " expecting " + expected);

			validateElement(element);
		}

		private void validateElement(Element element) {
			assertNotNull(element);
		}

		public void validate() {
			if (!expectedElements.isEmpty())
				fail(getSelectors() + " : Missing expected elements " + expectedElements);
		}
	}
}
