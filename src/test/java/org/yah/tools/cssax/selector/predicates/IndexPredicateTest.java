package org.yah.tools.cssax.selector.predicates;

import static junit.framework.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.yah.tools.cssax.selector.predicates.IndexPredicate;

public class IndexPredicateTest {

	@Test
	public void testEvaluate() {
		assertPredicate(2, 0, 8, 2, 4, 6, 8);
		assertPredicate(2, 1, 8, 1, 3, 5, 7);
		assertPredicate(2, 2, 8, 2, 4, 6, 8);
		assertPredicate(2, 3, 8, 3, 5, 7);
		assertPredicate(2, -3, 8, 1, 3, 5, 7);
		assertPredicate(-2, 0, 8);
		assertPredicate(-2, 2, 8, 2);
		assertPredicate(-1, 2, 8, 1, 2);
		assertPredicate(1, 2, 8, 2, 3, 4, 5, 6, 7, 8);
		assertPredicate(-2, -2, 8);
		assertPredicate(0, -2, 8);
		assertPredicate(0, 1, 8, 1);
		assertPredicate(0, 3, 8, 3);
		assertPredicate(0, -1, 8);
		assertPredicate(0, 0, 8);
	}

	private void assertPredicate(int a, int b, int count, int... expecteds) {
		Set<Integer> expectedsSet = new HashSet<Integer>();
		for (int i = 0; i < expecteds.length; i++)
			expectedsSet.add(Integer.valueOf(expecteds[i]));
		IndexPredicate predicate = new IndexPredicate(a, b);
		for (int i = 1; i <= count; i++) {
			boolean e = predicate.evaluate(i);
			if (e && !expectedsSet.remove(i))
				fail("Unexpected matched index " + i + " by predicate " + predicate);
		}
		if (!expectedsSet.isEmpty())
			fail("Unmatched index " + expectedsSet + " by predicate " + predicate);
	}
}
