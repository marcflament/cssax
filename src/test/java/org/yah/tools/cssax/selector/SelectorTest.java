package org.yah.tools.cssax.selector;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.yah.tools.cssax.selector.Selector;
import org.yah.tools.cssax.selector.SelectorParsingException;
import org.yah.tools.cssax.selector.Selectors;

public class SelectorTest {

	@Before
	public void setUp() throws Exception {
	}

	// * /* a=0 b=0 c=0 -> specificity = 0 */
	// LI /* a=0 b=0 c=1 -> specificity = 1 */
	// UL LI /* a=0 b=0 c=2 -> specificity = 2 */
	// UL OL+LI /* a=0 b=0 c=3 -> specificity = 3 */
	// H1 + *[REL=up] /* a=0 b=1 c=1 -> specificity = 11 */
	// UL OL LI.red /* a=0 b=1 c=3 -> specificity = 13 */
	// LI.red.level /* a=0 b=2 c=1 -> specificity = 21 */
	// #x34y /* a=1 b=0 c=0 -> specificity = 100 */
	// #s12:not(FOO) /* a=1 b=0 c=1 -> specificity = 101 */
	@Test
	public void testGetSpecificity() throws SelectorParsingException {
		assertEquals("000", formatSpecificity(selector("*")));
		assertEquals("001", formatSpecificity(selector("LI")));
		assertEquals("002", formatSpecificity(selector("UL LI")));
		assertEquals("003", formatSpecificity(selector("UL OL+LI")));
		assertEquals("011", formatSpecificity(selector("H1 + *[REL=up]")));
		assertEquals("013", formatSpecificity(selector("UL OL LI.red")));
		assertEquals("021", formatSpecificity(selector("LI.red.level")));
		assertEquals("100", formatSpecificity(selector("#x34y")));
		assertEquals("101", formatSpecificity(selector("#s12:not(FOO)")));
	}

	private String formatSpecificity(Selector selector) {
		StringBuilder sb = new StringBuilder();
		int[] specificity = selector.getSpecificity();
		for (int i = 0; i < specificity.length; i++) 
			sb.append(specificity[i]);
		return sb.toString();
	}

	private Selector selector(String definition) throws SelectorParsingException {
		Selectors selectors = new Selectors(definition);
		return selectors.getSelectors().get(0);
	}
}
