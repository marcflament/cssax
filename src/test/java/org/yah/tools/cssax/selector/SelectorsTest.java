package org.yah.tools.cssax.selector;

import static junit.framework.Assert.*;

import org.junit.Test;
import org.yah.tools.cssax.selector.SelectorParsingException;
import org.yah.tools.cssax.selector.Selectors;

public class SelectorsTest {

	@Test
	public void testParse() throws SelectorParsingException {
		Selectors selectors = new Selectors("div");
		assertEquals("div", selectors.toString());

		selectors = new Selectors("div#foo");
		assertEquals("div#foo", selectors.toString());

		selectors = new Selectors("div#foo,h2");
		assertEquals("div#foo, h2", selectors.toString());

		selectors = new Selectors("div#foo       ,             h2.class");
		assertEquals("div#foo, h2.class", selectors.toString());

		selectors = new Selectors("div[test]");
		assertEquals("div[test]", selectors.toString());

		selectors = new Selectors("* .test");
		assertEquals("* .test", selectors.toString());

		selectors = new Selectors("div[test=A]");
		assertEquals("div[test=A]", selectors.toString());

		selectors = new Selectors("div[test='A']");
		assertEquals("div[test=A]", selectors.toString());

		selectors = new Selectors("div[test=\"A\"]");
		assertEquals("div[test=A]", selectors.toString());

		selectors = new Selectors("div .test");
		assertEquals("div .test", selectors.toString());

		selectors = new Selectors("div         .test");
		assertEquals("div .test", selectors.toString());

		selectors = new Selectors("div span:first-child");
		assertEquals("div span:first-child", selectors.toString());

		selectors = new Selectors("DIV span:FIRst-child");
		assertEquals("DIV span:first-child", selectors.toString());

		try {
			selectors = new Selectors("div#");
			fail("Should have failed");
		} catch (SelectorParsingException e) {
			// expected
		}
		
		selectors = new Selectors("div:nth-child( 4n+1 )");
		assertEquals("div:nth-child(4n+1)", selectors.toString());

		selectors = new Selectors("div:nth-child(		4n-1)");
		assertEquals("div:nth-child(4n-1)", selectors.toString());

		selectors = new Selectors("div:nth-child(4)");
		assertEquals("div:nth-child(0n+4)", selectors.toString());

		selectors = new Selectors("div:nth-child(odd)");
		assertEquals("div:nth-child(2n+1)", selectors.toString());

		selectors = new Selectors("div:nth-child(even)");
		assertEquals("div:nth-child(2n+0)", selectors.toString());

		selectors = new Selectors("div:not( .test )");
		assertEquals("div:not(.test)", selectors.toString());

	}

}
