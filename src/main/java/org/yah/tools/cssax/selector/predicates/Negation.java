/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

import org.yah.tools.cssax.selector.Element;

/**
 * @author Yah
 *
 */
public class Negation implements ElementPredicate {
	
	private final ElementPredicate predicate;

	public Negation(ElementPredicate predicate) {
		super();
		this.predicate = predicate;
	}

	@Override
	public boolean evaluate(Element element) {
		return !predicate.evaluate(element);
	}

	@Override
	public String toString() {
		return String.format(":not(%s)", predicate);
	}
}
