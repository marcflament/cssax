package org.yah.tools.cssax.selector;

public interface SelectorListener {

	public void elementMatched(Element element) throws HtmlParsingException;

}