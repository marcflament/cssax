package org.yah.tools.cssax.selector;

import static org.yah.tools.cssax.selector.SelectorParser.*;
import org.yah.tools.cssax.selector.predicates.ElementPredicate;

class SelectorPart {
	private final int combinator;

	private final ElementPredicate[] predicates;

	public SelectorPart(int combinator, ElementPredicate[] predicates) {
		super();
		this.combinator = combinator;
		this.predicates = predicates;
	}

	public int getCombinator() {
		return combinator;
	}

	public ElementPredicate[] getPredicates() {
		return predicates;
	}

	public boolean evaluate(Element element) {
		for (ElementPredicate predicate : predicates) {
			if (!predicate.evaluate(element))
				return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (ElementPredicate predicate : predicates) {
			sb.append(predicate.toString());
		}
		return sb.toString();
	}

	public boolean isSibling() {
		return combinator == ADJACENT_SIBLING || combinator == GENERAL_SIBLING;
	}

	public boolean isDescendant() {
		return combinator == DESCENDANT;
	}

	public boolean isChild() {
		return combinator == CHILD;
	}

	public boolean isAdjacentSibling() {
		return combinator == ADJACENT_SIBLING;
	}

}