/**
 * 
 */
package org.yah.tools.cssax.selector;

import static org.yah.tools.cssax.selector.SelectorParser.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.yah.tools.cssax.selector.predicates.AttributePredicate;
import org.yah.tools.cssax.selector.predicates.CssClassPredicate;
import org.yah.tools.cssax.selector.predicates.ElementPredicate;
import org.yah.tools.cssax.selector.predicates.IdPredicate;
import org.yah.tools.cssax.selector.predicates.Negation;
import org.yah.tools.cssax.selector.predicates.PseudoPredicates;
import org.yah.tools.cssax.selector.predicates.TypePredicate;
import org.yah.tools.cssax.selector.predicates.UniversalPredicate;

/**
 * A list of Selector created from the definition of selector separated by a comma (ie : "div , #foo")
 * @author Yah
 */
public class Selectors implements Iterable<Selector> {

	private static final int ID_SPECIFICITY_INDEX = 0;
	private static final int ATT_SPECIFICITY_INDEX = 1;
	private static final int PSEUDO_SPECIFICITY_INDEX = 1;
	private static final int CLASS_SPECIFICITY_INDEX = 1;
	private static final int TYPE_SPECIFICITY_INDEX = 2;

	protected static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private final String definition;

	private final List<Selector> selectors;

	/**
	 * 
	 */
	public Selectors(String definition) throws SelectorParsingException {
		this.definition = definition;
		this.selectors = parseDefinition();
	}

	public List<Selector> getSelectors() {
		return selectors;
	}

	public String getDefinition() {
		return definition;
	}

	private List<Selector> parseDefinition() throws SelectorParsingException {
		List<Selector> res = new ArrayList<Selector>();
		final StringBuilder errors = new StringBuilder();
		SelectorLexer lexer = new SelectorLexer(new ANTLRStringStream(definition)) {
			@Override
			public void emitErrorMessage(String msg) {
				errors.append(LINE_SEPARATOR);
				errors.append(msg);
			}
		};
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		SelectorParser parser = new SelectorParser(tokenStream) {
			@Override
			public void emitErrorMessage(String msg) {
				errors.append(LINE_SEPARATOR);
				errors.append(msg);
			}
		};

		CommonTree tree;
		try {
			tree = (CommonTree) parser.selectors().getTree();
		} catch (RecognitionException e) {
			throw new SelectorParsingException("Parsing error of definition '" + definition + "'", e);
		}

		if (errors.length() > 0)
			throw new SelectorParsingException("Syntax error in '" + definition + "' : " + errors);

		for (int i = 0; i < tree.getChildCount(); i++) {
			CommonTree selectorNode = (CommonTree) tree.getChild(i);
			Selector selector = createSelector(selectorNode);
			res.add(selector);
		}
		return Collections.unmodifiableList(res);
	}

	private Selector createSelector(CommonTree selectorNode) throws SelectorParsingException {
		int[] predicateCount = new int[3];
		SelectorPart[] parts = createParts(selectorNode, predicateCount);
		return new Selector(parts, predicateCount);
	}

	private SelectorPart[] createParts(CommonTree selectorNode, int[] predicateCount) throws SelectorParsingException {
		SelectorPart[] res = new SelectorPart[selectorNode.getChildCount()];
		for (int i = 0; i < selectorNode.getChildCount(); i++) {
			CommonTree child = (CommonTree) selectorNode.getChild(i);
			res[i] = createPart(child, predicateCount);
		}
		return res;
	}

	private SelectorPart createPart(CommonTree partNode, int[] predicateCount) throws SelectorParsingException {
		ElementPredicate[] predicates = new ElementPredicate[partNode.getChildCount()];
		for (int i = 0; i < partNode.getChildCount(); i++) {
			CommonTree predicateNode = (CommonTree) partNode.getChild(i);
			predicates[i] = createPredicate(predicateNode, predicateCount);
		}
		return new SelectorPart(partNode.getType(), predicates);
	}

	private ElementPredicate createPredicate(CommonTree predicateNode, int[] predicateCount)
			throws SelectorParsingException {
		ElementPredicate predicate;
		switch (predicateNode.getType()) {
		case UNIVERSAL:
			predicate = new UniversalPredicate();
			break;
		case TYPE:
			predicate = new TypePredicate(predicateNode.getText());
			predicateCount[TYPE_SPECIFICITY_INDEX]++;
			break;
		case ID:
			String id = predicateNode.getText().substring(1);
			predicate = new IdPredicate(id);
			predicateCount[ID_SPECIFICITY_INDEX]++;
			break;
		case CLASS:
			predicate = new CssClassPredicate(predicateNode.getText());
			predicateCount[CLASS_SPECIFICITY_INDEX]++;
			break;
		case ATTRIB:
			if (predicateNode.getChildCount() > 0) {
				int operator = predicateNode.getChild(0).getType();
				String value = predicateNode.getChild(1).getText();
				if (predicateNode.getChild(1).getType() == STRING)
					value = value.substring(1, value.length() - 1);
				predicate = new AttributePredicate(predicateNode.getText(), operator, value);
			} else
				predicate = new AttributePredicate(predicateNode.getText());
			predicateCount[ATT_SPECIFICITY_INDEX]++;
			break;
		case PSEUDO:
			predicate = createPseudoPredicate(predicateNode);
			predicateCount[PSEUDO_SPECIFICITY_INDEX]++;
			break;
		case NOT:
			predicate = new Negation(createPredicate((CommonTree) predicateNode.getChild(0), predicateCount));
			break;
		default:
			throw new SelectorParsingException("Unhandled predicate " + tokenNames[predicateNode.getType()]);
		}
		return predicate;
	}

	private ElementPredicate createPseudoPredicate(CommonTree predicateNode) throws SelectorParsingException {
		String tokenText = predicateNode.getText();
		PseudoClass pseudoClass = PseudoClass.fromToken(tokenText);
		if (pseudoClass == null)
			throw new SelectorParsingException("Unknown pseudo class " + tokenText);
		switch (pseudoClass) {
		case ROOT:
			return PseudoPredicates.ROOT_PREDICATE;
		case FIRST_CHILD:
			return PseudoPredicates.FIRST_CHILD_PREDICATE;
		case LAST_CHILD:
			return PseudoPredicates.LAST_CHILD_PREDICATE;
		case FIRST_OF_TYPE:
			return PseudoPredicates.FIRST_OF_TYPE_PREDICATE;
		case NTH_CHILD:
		case NTH_OF_TYPE:
			if (predicateNode.getChildCount() == 0)
				throw new SelectorParsingException("Missing argument for pseudo class " + pseudoClass);
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < predicateNode.getChildCount(); i++) {
				sb.append(predicateNode.getChild(i).getText());
			}
			if (pseudoClass == PseudoClass.NTH_CHILD)
				return new PseudoPredicates.NthChildPredicate(sb.toString());
			else
				return new PseudoPredicates.NthOfTypePredicate(sb.toString());
		default:
			throw new SelectorParsingException("Unsupported pseudo class " + pseudoClass);
		}
	}

	@Override
	public Iterator<Selector> iterator() {
		return selectors.iterator();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Selectors other = (Selectors) obj;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<Selector> iter = selectors.iterator();
		while (iter.hasNext()) {
			sb.append(iter.next());
			if (iter.hasNext())
				sb.append(", ");
		}
		return sb.toString();
	}
}
