/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.yah.tools.cssax.selector.Element;
import org.yah.tools.cssax.selector.SelectorParsingException;

/**
 * @author Yah
 * 
 */
public class PseudoPredicates {

	public static final ElementPredicate FIRST_CHILD_PREDICATE = new FirstChildPredicate();
	public static final ElementPredicate FIRST_OF_TYPE_PREDICATE = new FirstOfTypePredicate();
	public static final ElementPredicate LAST_CHILD_PREDICATE = new LastChildPredicate();
	public static final ElementPredicate ROOT_PREDICATE = new RootPredicate();

	private static final Pattern CHILD_INDEX_PATTERN = Pattern
			.compile("((?:-|\\+)?\\d*)(?:n|N)(\\s*(?:-|\\+)\\s*\\d+)?");

	/**
	 * 
	 */
	private PseudoPredicates() {
	}

	private static IndexPredicate createIndexPredicate(String argument) throws SelectorParsingException {
		String arg = argument.toLowerCase();
		if ("odd".equals(arg))
			return new IndexPredicate(2, 1);
		if ("even".equals(arg))
			return new IndexPredicate(2, 0);

		try {
			int b = Integer.parseInt(arg);
			return new IndexPredicate(0, b);
		} catch (NumberFormatException e) {
			Matcher matcher = CHILD_INDEX_PATTERN.matcher(argument);
			if (matcher.matches()) {
				int a = Integer.valueOf(matcher.group(1));
				int b = Integer.valueOf(matcher.group(2));
				return new IndexPredicate(a, b);
			}
			throw new SelectorParsingException("Invalid index argument " + argument);
		}
	}

	public static class FirstChildPredicate implements ElementPredicate {
		@Override
		public boolean evaluate(Element element) {
			return element.getIndex() == 0;
		}

		@Override
		public String toString() {
			return ":first-child";
		}
	}

	public static class LastChildPredicate implements ElementPredicate {
		@Override
		public boolean evaluate(Element element) {
			return element.getIndex() == (element.getParent().getChildCount() - 1);
		}

		@Override
		public String toString() {
			return ":last-child";
		}
	}

	public static class NthChildPredicate implements ElementPredicate {

		private IndexPredicate indexPredicate;

		public NthChildPredicate(String argument) throws SelectorParsingException {
			super();
			this.indexPredicate = createIndexPredicate(argument);
		}

		@Override
		public boolean evaluate(Element element) {
			return indexPredicate.evaluate(element.getIndex() + 1);
		}

		@Override
		public String toString() {
			return ":nth-child(" + indexPredicate.toString() + ")";
		}

	}

	public static class FirstOfTypePredicate implements ElementPredicate {
		@Override
		public boolean evaluate(Element element) {
			return element.getTagIndex() == 0;
		}

		@Override
		public String toString() {
			return ":first-of-type";
		}
	}

	public static class NthOfTypePredicate implements ElementPredicate {

		private IndexPredicate indexPredicate;

		public NthOfTypePredicate(String argument) throws SelectorParsingException {
			super();
			this.indexPredicate = createIndexPredicate(argument);
		}

		@Override
		public boolean evaluate(Element element) {
			return indexPredicate.evaluate(element.getTagIndex() + 1);
		}

		@Override
		public String toString() {
			return ":nth-of-type(" + indexPredicate.toString() + ")";
		}

	}

	public static class RootPredicate implements ElementPredicate {
		@Override
		public boolean evaluate(Element element) {
			return element.getParent() == null;
		}

		@Override
		public String toString() {
			return ":root";
		}
	}

}
