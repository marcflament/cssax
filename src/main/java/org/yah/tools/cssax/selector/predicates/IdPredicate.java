/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

/**
 * @author Yah
 * 
 */
public class IdPredicate extends AttributePredicate {

	/**
	 * @param attributeName
	 * @param value
	 */
	public IdPredicate(String value) {
		super("id", value);
	}

	@Override
	public String toString() {
		return "#" + value;
	}

}
