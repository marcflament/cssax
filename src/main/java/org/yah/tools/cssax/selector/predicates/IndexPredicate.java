package org.yah.tools.cssax.selector.predicates;

public class IndexPredicate {
	private final int a;
	private final int b;

	public IndexPredicate(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}

	public boolean evaluate(int index) {
		if (a == 0)
			return index == b;
		int relative = index - b;
		int n = relative / a;
		int next = a * n + b;
		return n >= 0 && next == index;
	}

	@Override
	public String toString() {
		return String.format("%dn%+d", a, b);
	}
}