/**
 * 
 */
package org.yah.tools.cssax.selector;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A cache of parsed CSS selector.
 * 
 * @author Yah
 */
public class SelectorsCache {

	private final ConcurrentMap<String, Selectors> selectorsCache = new ConcurrentHashMap<String, Selectors>();

	/**
	 * 
	 */
	public SelectorsCache() {
		super();
	}

	public Selectors getSelector(String definition) throws SelectorParsingException {
		Selectors res = selectorsCache.get(definition);
		if (res == null) {
			Selectors previous = selectorsCache.putIfAbsent(definition, res = new Selectors(definition));
			if (previous != null)
				res = previous;
		}
		return res;
	}

	public void clear() {
		selectorsCache.clear();
	}

	public int size() {
		return selectorsCache.size();
	}

}
