/**
 * 
 */
package org.yah.tools.cssax.selector;

/**
 * @author Yah
 * 
 */
public class HtmlParsingException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public HtmlParsingException() {
		super();
	}

	/**
	 * @param message
	 */
	public HtmlParsingException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HtmlParsingException(String message, Throwable cause) {
		super(message, cause);
	}

}
