package org.yah.tools.cssax.selector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import org.yah.tools.cssax.selector.SaxHtmlExtractor.Selection;

class ParsingContext {

	private List<SelectorContext> selectorContexts;

	private final Stack<Map<SelectorContext, Selection>> selections = new Stack<Map<SelectorContext, Selection>>();

	private final Map<Selector, AtomicInteger> matchedSelectors = new LinkedHashMap<Selector, AtomicInteger>();

	private final Map<Selector, List<Element>> matchedElements = new LinkedHashMap<Selector, List<Element>>();

	private final Set<Selector> skippedSelectors = new HashSet<Selector>();

	private final Set<SelectorBinding> matchedBindings = new HashSet<SelectorBinding>();

	Element element;

	public ParsingContext(SelectorBinding... bindings) {
		this.selectorContexts = new ArrayList<SelectorContext>();
		int order = 0;
		for (SelectorBinding selectorsBinding : bindings) {
			for (Selector selector : selectorsBinding.getSelectors()) {
				SelectorContext selectorContext = findContext(selector);
				if (selectorContext == null)
					selectorContexts.add(selectorContext = new SelectorContext(selector, order++));
				selectorContext.addBinding(selectorsBinding);
			}
		}
		Collections.sort(selectorContexts);
	}

	private SelectorContext findContext(Selector selector) {
		for (SelectorContext selectorContext : this.selectorContexts) {
			if (selectorContext.selector == selector)
				return selectorContext;
		}
		return null;
	}

	public void start(Element element) {
		this.element = element;
		Map<SelectorContext, Selection> currentSelections = new LinkedHashMap<SelectorContext, Selection>();
		for (SelectorContext context : selectorContexts) {
			Selector selector = context.selector;
			if (skipped(selector))
				continue;

			Selection selection = context.evaluate(element);
			currentSelections.put(context, selection);

			if (selection == Selection.SKIP)
				skippedSelectors.add(selector);
			else if (selection == Selection.MATCHED) {
				addMatchedSelector(selector, element);
			}
		}
		selections.push(currentSelections);
	}

	private void addMatchedSelector(Selector selector, Element element) {
		AtomicInteger current = matchedSelectors.get(selector);
		if (current == null) {
			matchedSelectors.put(selector, new AtomicInteger(1));
			List<Element> elements = new LinkedList<Element>();
			elements.add(element);
			matchedElements.put(selector, elements);
		} else {
			current.incrementAndGet();
			matchedElements.get(selector).add(element);
		}
	}

	private List<Element> removeMatchedSelector(Selector selector) {
		AtomicInteger current = matchedSelectors.get(selector);
		if (current == null)
			throw new IllegalArgumentException("Selector " + selector + " not matched");
		int remainging = current.decrementAndGet();
		if (remainging == 0) {
			List<Element> elements = matchedElements.remove(selector);
			matchedSelectors.remove(selector);
			return elements;
		}
		return null;
	}

	public void end() throws HtmlParsingException {
		Map<SelectorContext, Selection> tagSelections = selections.pop();
		for (Map.Entry<SelectorContext, Selection> entry : tagSelections.entrySet()) {
			SelectorContext selectorContext = entry.getKey();
			Selector selector = selectorContext.selector;
			selectorContext.elementComplete(element, entry.getValue());
			if (entry.getValue() == Selection.MATCHED) {
				List<Element> matchedElements = removeMatchedSelector(selector);
				if (matchedElements != null) {
					ListIterator<Element> iter = matchedElements.listIterator();
					while (iter.hasNext()) {
						Element matchedElement = iter.next();
						for (SelectorBinding selectorsBinding : selectorContext.bindings) {
							selectorsBinding.elementMatched(matchedElement);
							matchedBindings.add(selectorsBinding);
						}
					}
				}
			} else if (entry.getValue() == Selection.SKIP) {
				skippedSelectors.remove(selector);
			}
		}
		element = element.getParent();
	}

	public void validate() throws HtmlParsingException {
		// check expected bindings
		for (SelectorContext selectorContext : selectorContexts) {
			for (SelectorBinding selectorBinding : selectorContext.bindings) {
				if (selectorBinding.isExpected() && !matchedBindings.contains(selectorBinding))
					throw new HtmlParsingException("Expected selector " + selectorBinding + " was not matched");
			}
		}
	}

	public boolean matched() {
		return !matchedSelectors.isEmpty();
	}

	boolean skipped() {
		return skippedSelectors.size() == selectorContexts.size();
	}

	private boolean skipped(Selector selector) {
		return skippedSelectors.contains(selector);
	}

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private class SelectorContext implements Comparable<SelectorContext> {

		private final int order;

		private final Selector selector;

		private final List<SelectorBinding> bindings;

		private final LinkedList<Element>[] matchedParts;

		private int partIndex;

		@SuppressWarnings("unchecked")
		public SelectorContext(Selector selector, int order) {
			super();
			this.order = order;
			this.selector = selector;
			this.bindings = new LinkedList<SelectorBinding>();
			this.matchedParts = new LinkedList[selector.getSelectorParts().length];
			this.partIndex = 0;
		}

		public void addBinding(SelectorBinding binding) {
			bindings.add(binding);
		}

		public Selection evaluate(Element element) {
			int lastPartIndex = getLastPartIndex();
			SelectorPart[] selectorParts = selector.getSelectorParts();
			SelectorPart selectorPart = selectorParts[lastPartIndex];
			boolean matched = evaluatePart(element, lastPartIndex);

			if (matched || selectorPart.isDescendant())
				addMatchedPart(element);
			else if (selectorPart.isSibling()) {
				// for unmatched sibling, try to match last selector part to
				// restart sibling matching
				int index = lastPartIndex - 1;
				while (index >= 0) {
					SelectorPart previousPart = selectorParts[index];
					boolean previousMatch = evaluatePart(element, index);
					if (previousMatch) {
						for (int i = index + 1; i < matchedParts.length; i++)
							matchedParts[i] = null;
						matchedParts[index].pollLast();
						matchedParts[index].add(element);
						return Selection.SKIP;
					}
					if (previousPart.isSibling())
						index--;
					else
						break;
				}
			}

			if (matched && nextPart())
				return Selection.MATCHED;

			// check for next selector and skip children if next part is
			// combined with sibling
			if (matched && lastPartIndex < selectorParts.length - 1) {
				SelectorPart nextPart = selectorParts[lastPartIndex + 1];
				if (nextPart.isSibling())
					return Selection.SKIP;
			}

			// always keep searching for descendant
			if (selectorPart.isDescendant())
				return Selection.MATCHING;

			// stop if child/sibling does not match
			return matched ? Selection.MATCHING : Selection.SKIP;
		}

		private boolean evaluatePart(Element element, int partIndex) {
			SelectorPart selectorPart = selector.getSelectorParts()[partIndex];
			boolean matched = selectorPart.evaluate(element);
			if (!matched)
				return false;

			if (selectorPart.isDescendant())
				return true;

			// since we are combined with sibling or child, there is a
			// previous matched part
			Element lastMatchedElement = matchedParts[partIndex - 1].getLast();

			if (selectorPart.isChild()) {
				// check that last matched element of previous part is our
				// parent
				return lastMatchedElement == element.getParent();
			} else if (selectorPart.isAdjacentSibling()) {
				return element.getIndex() == (lastMatchedElement.getIndex() + 1);
			} else {
				// general sibling combinator
				return element.getIndex() > lastMatchedElement.getIndex();
			}
		}

		public void elementComplete(Element element, Selection selection) throws HtmlParsingException {
			int completedPartIndex;
			SelectorPart[] selectorParts = selector.getSelectorParts();
			switch (selection) {
			case MATCHED:
				// when matched, part index is always after last selector part
				// (selectorParts.length)
				completedPartIndex = selectorParts.length - 1;
				removeMatchedPart(completedPartIndex);
				// revert matching status by returning to last part
				partIndex = completedPartIndex;
				break;
			case MATCHING:
				int currentPartIndex = getLastPartIndex();
				for (completedPartIndex = currentPartIndex; completedPartIndex >= 0; completedPartIndex--) {
					LinkedList<Element> parts = matchedParts[completedPartIndex];
					// TODO : optimize search in list by starting from end
					if (parts != null && parts.contains(element))
						break;
				}
				if (completedPartIndex < 0)
					throw new HtmlParsingException("No parts found matching element " + element);

				if (completedPartIndex < currentPartIndex) {
					// end matching current part (and parent parts until
					// completed part)
					// cleanup matched parts after completedPart and until
					// matchingPartIndex
					for (int i = completedPartIndex + 1; i <= currentPartIndex; i++)
						matchedParts[i] = null;
				}
				removeMatchedPart(completedPartIndex);
				partIndex = completedPartIndex;
				break;
			default:
				break;
			}
		}

		private boolean nextPart() {
			if (partIndex < selector.getSelectorParts().length)
				partIndex++;
			return matchingComplete();
		}

		private boolean removeMatchedPart(int index) {
			matchedParts[index].pollLast();
			if (matchedParts[index].isEmpty()) {
				matchedParts[index] = null;
				return true;
			}
			return false;
		}

		private void addMatchedPart(Element element) {
			int lastPartIndex = getLastPartIndex();
			if (matchedParts[lastPartIndex] == null)
				matchedParts[lastPartIndex] = new LinkedList<Element>();
			matchedParts[lastPartIndex].add(element);
		}

		private int getLastPartIndex() {
			if (matchingComplete())
				return partIndex - 1;
			return partIndex;
		}

		private boolean matchingComplete() {
			return partIndex == selector.getSelectorParts().length;
		}

		private SelectorPart getSelectorPart(int index) {
			return selector.getSelectorParts()[index];
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append(LINE_SEPARATOR);
			for (int i = 0; i < matchedParts.length; i++) {
				builder.append("\t");
				if (i == partIndex)
					builder.append('*');
				builder.append(getSelectorPart(i)).append(" : ");
				builder.append(matchedParts[i] == null ? "null" : matchedParts[i].getLast());
				if (i == partIndex)
					builder.append('*');
				if (i < matchedParts.length - 1)
					builder.append(LINE_SEPARATOR);
			}
			return builder.toString();
		}

		@Override
		public int compareTo(SelectorContext o) {
			if (o.selector == selector)
				return 0;

			int[] specificity = selector.getSpecificity();
			int[] oSpecificity = o.selector.getSpecificity();
			for (int i = 0; i < specificity.length; i++) {
				if (oSpecificity[i] > specificity[i])
					return 1;
				else if (oSpecificity[i] < specificity[i])
					return -1;
			}
			return order < o.order ? 1 : -1;
		}

	}
}