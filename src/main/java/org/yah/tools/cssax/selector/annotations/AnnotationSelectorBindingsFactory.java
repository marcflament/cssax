/**
 * 
 */
package org.yah.tools.cssax.selector.annotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yah.tools.cssax.selector.Element;
import org.yah.tools.cssax.selector.SaxHtmlExtractor;
import org.yah.tools.cssax.selector.HtmlParsingException;
import org.yah.tools.cssax.selector.SelectorBinding;
import org.yah.tools.cssax.selector.SelectorBindingsBuilder;
import org.yah.tools.cssax.selector.SelectorListener;
import org.yah.tools.cssax.selector.SelectorParsingException;
import org.yah.tools.cssax.selector.Selectors;
import org.yah.tools.cssax.selector.SelectorsCache;
import org.yah.tools.cssax.selector.SelectorParser.attrib_param_return;

/**
 * A factory of {@link SelectorBinding} from {@link attrib_param_return} bean
 * with annotated method by {@link Selector} annotation.
 * 
 * @author Yah
 * 
 */
public class AnnotationSelectorBindingsFactory {

	private static final Logger logger = LoggerFactory.getLogger(AnnotationSelectorBindingsFactory.class);

	private final ConcurrentMap<Class<?>, BindingsModel> modelsCache = new ConcurrentHashMap<Class<?>, BindingsModel>();

	private SelectorsCache selectorsCache;

	public AnnotationSelectorBindingsFactory() {}

	/**
	 * @param selectorsCache
	 *            a cache used for parsed selector
	 */
	public AnnotationSelectorBindingsFactory(SelectorsCache selectorsCache) {
		super();
		this.selectorsCache = selectorsCache;
	}

	public void setSelectorsCache(SelectorsCache selectorsCache) {
		this.selectorsCache = selectorsCache;
	}

	/**
	 * @param bean
	 *            The bean containing methods annotated with {@link Selector}
	 * @param methods
	 *            The method name to include, if none, all annotated methods
	 *            will be used.
	 * @return The {@link SelectorBinding}s to use with {@link SaxHtmlExtractor}
	 * @throws SelectorParsingException
	 */
	public SelectorBinding[] createBindings(Object bean, String... methods) throws SelectorParsingException {
		if (bean == null)
			throw new IllegalArgumentException("bean is null");

		BindingsModel model = modelsCache.get(bean.getClass());
		if (model == null) {
			BindingsModel previous = modelsCache.putIfAbsent(bean.getClass(),
					model = new BindingsModel(bean.getClass()));
			if (previous != null)
				model = previous;
		}

		return model.createBindings(bean, methods);
	}

	private Selectors getSelectors(String definition) throws SelectorParsingException {
		if (selectorsCache != null)
			return selectorsCache.getSelector(definition);
		return new Selectors(definition);
	}

	private class BindingsModel {
		private final Map<Selectors, ListenerMethod> boundMethods;

		public BindingsModel(Class<?> model) throws SelectorParsingException {
			Method[] methods = model.getMethods();
			this.boundMethods = new HashMap<Selectors, ListenerMethod>();
			for (int i = 0; i < methods.length; i++) {
				Method method = methods[i];
				Selector annotation = method.getAnnotation(Selector.class);
				if (annotation != null) {
					Selectors selectors = getSelectors(annotation.value());
					boundMethods.put(selectors, new ListenerMethod(method, annotation.expected()));
				}
			}
		}

		public SelectorBinding[] createBindings(Object bean, String[] methods) {
			SelectorBindingsBuilder builder = new SelectorBindingsBuilder();
			Set<String> filteredMethods;
			if (methods == null || methods.length == 0)
				filteredMethods = null;
			else
				filteredMethods = new HashSet<String>(Arrays.asList(methods));
			for (Map.Entry<Selectors, ListenerMethod> entry : boundMethods.entrySet()) {
				ListenerMethod method = entry.getValue();
				if (filteredMethods == null || filteredMethods.contains(method.getName()))
					builder.addBinding(
							new SelectorBinding(entry.getKey(), method.createListener(bean), method.expected));
			}
			return builder.build();
		}
	}

	private class ListenerMethod {

		private final Method method;

		private final boolean expectElement;

		private final boolean expected;

		public ListenerMethod(Method method, boolean expected) {
			super();
			this.method = method;
			Class<?>[] parameterTypes = method.getParameterTypes();
			if (parameterTypes.length > 1)
				throw new IllegalArgumentException("Method '" + method + "' as more parameters than expected");
			if (parameterTypes.length > 0) {
				if (Element.class.equals(parameterTypes[0]))
					expectElement = true;
				else
					throw new IllegalArgumentException("Invalid parameter type '" + parameterTypes[0].getName()
							+ "' for selector ethod " + method);
			} else
				expectElement = false;
			this.expected = expected;
		}

		public SelectorListener createListener(final Object bean) {
			return new SelectorListener() {
				@Override
				public void elementMatched(Element element) throws HtmlParsingException {
					logger.debug("{} matched by method {}", element, method.getName());
					try {
						method.invoke(bean, expectElement ? new Object[] { element } : null);
					} catch (InvocationTargetException e) {
						if (e.getTargetException() instanceof HtmlParsingException)
							throw (HtmlParsingException) e.getTargetException();
						throw new HtmlParsingException("Exception occured while processing matched element",
								e.getTargetException());
					} catch (Exception e) {
						throw new HtmlParsingException("Exception occured while invoking method " + method, e);
					}
				}
			};
		}

		public String getName() {
			return method.getName();
		}

	}
}
