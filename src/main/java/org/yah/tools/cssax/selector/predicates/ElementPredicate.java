package org.yah.tools.cssax.selector.predicates;

import org.yah.tools.cssax.selector.Element;

public interface ElementPredicate {
	
	public boolean evaluate(Element element);

}