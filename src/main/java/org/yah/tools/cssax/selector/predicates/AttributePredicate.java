/**
 * 
 */

package org.yah.tools.cssax.selector.predicates;

import static org.yah.tools.cssax.selector.SelectorParser.*;

import org.yah.tools.cssax.selector.Element;

/**
 * @author Yah
 * 
 */
public class AttributePredicate implements ElementPredicate {

	protected final String attributeName;

	protected final int operator;

	protected final String value;

	public AttributePredicate(String attributeName) {
		this(attributeName, -1, null);
	}

	public AttributePredicate(String attributeName, String value) {
		this(attributeName, EQUAL, value);
	}

	public AttributePredicate(String attributeName, int operator, String value) {
		super();
		this.attributeName = attributeName;
		this.operator = operator;
		this.value = value;
	}

	@Override
	public boolean evaluate(Element element) {
		if (value == null)
			return element.getAttributes().containsKey(attributeName);

		String elementValue = element.getAttribute(attributeName);
		if (elementValue == null)
			return false;
		switch (operator) {
		case EQUAL:
			return value.equals(elementValue);
		case INCLUDES:
			String[] words = elementValue.split("\\s+");
			for (int i = 0; i < words.length; i++) {
				if (words[i].equals(value))
					return true;
			}
			return false;
		case DASHMATCH:
			return elementValue.equals(value) || elementValue.startsWith(value + "-");
		case CARETMATCH:
			if (value.length() == 0)
				return false;
			return elementValue.startsWith(value);
		case DOLARMATCH:
			if (value.length() == 0)
				return false;
			return elementValue.endsWith(value);
		case STARMATCH:
			if (value.length() == 0)
				return false;
			return elementValue.contains(value);
		default:
			throw new IllegalStateException("Unsupported operator " + tokenNames[operator]);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(attributeName);
		if (value != null) {
			switch (operator) {
			case EQUAL:
				sb.append("=");
				break;
			case INCLUDES:
				sb.append("~=");
				break;
			case DASHMATCH:
				sb.append("|=");
				break;
			case CARETMATCH:
				sb.append("^=");
				break;
			case DOLARMATCH:
				sb.append("$=");
				break;
			case STARMATCH:
				sb.append("*=");
				break;
			default:
				throw new IllegalStateException("Unsupported operator " + tokenNames[operator]);
			}
			sb.append(value);
		}
		sb.append("]");
		return sb.toString();
	}
}
