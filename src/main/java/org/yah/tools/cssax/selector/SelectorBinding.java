/**
 * 
 */
package org.yah.tools.cssax.selector;

/**
 * A binding between {@link Selectors} and the {@link SelectorListener}<br/>
 * If no {@link SelectorListener} is provided, the elementMatched of the binding
 * MUST BE override.
 * 
 * @author Yah
 */
public class SelectorBinding implements SelectorListener {

	private final Selectors selectors;

	private final SelectorListener delegate;

	private boolean expected;

	/**
	 * @param definition
	 *            the CSS selectors definition
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(String definition) throws SelectorParsingException {
		this(definition, false);
	}

	/**
	 * @param definition
	 *            the CSS selectors definition
	 * @param expected
	 *            if true, an exception will be raised during the parsing if
	 *            this selector is not matched
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(String definition, boolean expected) throws SelectorParsingException {
		this(definition, null, expected);
	}

	/**
	 * @param definition
	 *            the CSS selectors definition
	 * @param delegate
	 *            a {@link SelectorListener} to use instead of this binding
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(String definition, SelectorListener delegate) throws SelectorParsingException {
		this(definition, delegate, false);
	}

	/**
	 * @param definition
	 *            the CSS selectors definition
	 * @param delegate
	 *            a {@link SelectorListener} to use instead of this binding
	 * @param expected
	 *            if true, an exception will be raised during the parsing if
	 *            this selector is not matched
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(String definition, SelectorListener delegate, boolean expected)
			throws SelectorParsingException {
		this(new Selectors(definition), delegate, expected);
	}

	/**
	 * @param selectors
	 *            the selectors to use
	 */
	public SelectorBinding(Selectors selectors) {
		this(selectors, false);
	}

	/**
	 * 
	 * @param selectors
	 *            the selectors to use
	 * @param expected
	 *            if true, an exception will be raised during the parsing if
	 *            this selector is not matched
	 */
	public SelectorBinding(Selectors selectors, boolean expected) {
		this(selectors, null, expected);
	}

	/**
	 * @param selectors
	 *            the selectors to use
	 * @param delegate
	 *            a {@link SelectorListener} to use instead of this binding
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(Selectors selectors, SelectorListener delegate) {
		this(selectors, null, false);
	}

	/**
	 * @param selectors
	 *            the selectors to use
	 * @param delegate
	 *            a {@link SelectorListener} to use instead of this binding
	 * @param expected
	 *            if true, an exception will be raised during the parsing if
	 *            this selector is not matched
	 * @throws SelectorParsingException
	 */
	public SelectorBinding(Selectors selectors, SelectorListener delegate, boolean expected) {
		super();
		this.selectors = selectors;
		this.delegate = delegate;
		this.expected = expected;
	}

	public Selectors getSelectors() {
		return selectors;
	}

	/* (non-Javadoc)
	 * @see org.yah.tools.cssax.selector.SelectorListener#elementMatched(org.yah.tools.cssax.selector.Element)
	 */
	@Override
	public void elementMatched(Element element) throws HtmlParsingException {
		if (delegate != null)
			delegate.elementMatched(element);
		else
			throw new ElementHandlerException("Must be overrided if no delegate is configured");
	}

	public boolean isExpected() {
		return expected;
	}

	@Override
	public String toString() {
		return selectors.toString();
	}
}
