package org.yah.tools.cssax.selector;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.xml.sax.Attributes;

/**
 * An HTML element provided when a selector is matched.<br/>
 * All children will be initialized when given to the selector listener. <br/>
 * Text content can be retrieved using getText()<br/>
 * A Selector can also be applied directly on a element using the find and
 * findFirst methods.
 * 
 * @author Yah
 */
public class Element {

	private static final String TEXT_TAG = "#text";

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private final Element parent;

	private final String tag;

	private final int[] location;

	private final int index;

	private final int tagIndex;

	private final Map<String, String> attributes;

	private int childCount;

	private final Map<String, Integer> tagsCount = new HashMap<String, Integer>();

	private List<Element> children;

	private String text;

	public Element(String tag, Attributes attributes, int[] location) {
		this(null, tag, 0, 0, attributes, location);
	}

	private Element(Element parent, String tag, int childIndex, int tagIndex, Attributes attributes, int[] location) {
		super();
		this.parent = parent;
		this.tag = tag;
		this.index = childIndex;
		this.tagIndex = tagIndex;
		this.attributes = createAttributes(attributes);
		this.location = location;
	}

	/**
	 * @return Element name (ie : p , div , span)
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * @return Element index in the parent children
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return The element line in the html stream
	 */
	public int getLine() {
		return location == null ? -1 : location[0];
	}

	/**
	 * @return The element column in the html stream
	 */
	public int getColumn() {
		return location == null ? -1 : location[1];
	}

	/**
	 * @return the index of this element in parent amongst children with same
	 *         tag name.
	 */
	public int getTagIndex() {
		return tagIndex;
	}

	/**
	 * @return count of children Element (does not include text child)
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @return
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	public String getAttribute(String name) {
		if (attributes == null || attributes.isEmpty())
			return null;
		return attributes.get(name);
	}

	public List<Element> getChildren() {
		if (children == null)
			return Collections.emptyList();
		return children;
	}

	public Element getParent() {
		return parent;
	}

	Element addChildTag(String tag, Attributes attributes, int[] location) {
		int childIndex = childCount;
		if (!TEXT_TAG.equals(tag))
			childCount++;
		int childTagIndex;
		if (!tagsCount.containsKey(tag))
			childTagIndex = 0;
		else
			childTagIndex = tagsCount.get(tag).intValue();
		tagsCount.put(tag, Integer.valueOf(childTagIndex + 1));
		return new Element(this, tag, childIndex, childTagIndex, attributes, location);
	}

	private Map<String, String> createAttributes(Attributes attributes) {
		if (attributes == null || attributes.getLength() == 0)
			return Collections.emptyMap();
		Map<String, String> res = new HashMap<String, String>();
		for (int i = 0; i < attributes.getLength(); i++) {
			res.put(attributes.getLocalName(i), attributes.getValue(i));
		}
		return res;
	}

	void addChild(Element child) {
		if (children == null)
			children = new LinkedList<Element>();
		children.add(child);
	}

	void cleanup() {
		children = null;
	}

	void addText(String text, int[] location) {
		Element child = addChildTag(TEXT_TAG, null, location);
		child.text = text;
		addChild(child);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Element current = this;
		while (current != null) {
			sb.insert(0, "]").insert(0, current.tagIndex).insert(0, "[");
			sb.insert(0, current.tag);
			current = current.parent;
			if (current != null)
				sb.insert(0, " > ");
		}
		return sb.toString();
	}

	/**
	 * @return the text inside this element
	 */
	public String getText() {
		StringBuilder sb = new StringBuilder();
		Iterator<Element> iter = getChildren().iterator();
		while (iter.hasNext()) {
			Element child = iter.next();
			if (child.text != null) {
				sb.append(child.text);
			}
		}
		return sb.toString().trim();
	}

	/**
	 * @return the text inside this element and all his descendant
	 */
	public String getFullText() {
		if (text != null)
			return text;

		StringBuilder sb = new StringBuilder();
		for (Element child : getChildren())
			sb.append(child.getFullText());
		return sb.toString().trim();
	}

	/**
	 * @param selector
	 * @return the descendant elements matching the provided selectors
	 * @throws HtmlParsingException
	 */
	public List<Element> find(Selectors selector) throws HtmlParsingException {
		ElementFinderBinding binding = new ElementFinderBinding(selector);
		ParsingContext context = new ParsingContext(binding);
		find(this, context);
		return binding.elements == null ? Collections.<Element>emptyList() : binding.elements;
	}

	/**
	 * @param selector
	 * @return the first descendant elements matching the provided selectors
	 * @throws HtmlParsingException
	 */
	public Element findFirst(Selectors selector) throws HtmlParsingException {
		ElementFinderBinding binding = new ElementFinderBinding(selector);
		ParsingContext context = new ParsingContext(binding);
		findFirst(this, context);
		return binding.elements == null ? null : binding.elements.get(0);
	}

	private void findFirst(Element element, ParsingContext context) throws HtmlParsingException {
		List<Element> children = element.getChildren();
		for (Element child : children) {
			context.start(child);
			if (!context.skipped())
				findFirst(child, context);
			boolean matched = context.matched();
			context.end();
			if (matched)
				break;
		}
	}

	private void find(Element element, ParsingContext context) throws HtmlParsingException {
		List<Element> children = element.getChildren();
		for (Element child : children) {
			context.start(child);
			if (!context.skipped())
				find(child, context);
			context.end();
		}
	}

	private class ElementFinderBinding extends SelectorBinding {
		private List<Element> elements;

		public ElementFinderBinding(Selectors selector) {
			super(selector);
		}

		@Override
		public void elementMatched(Element element) throws ElementHandlerException {
			if (elements == null)
				elements = new LinkedList<Element>();
			elements.add(element);
		}
	}

	public String toTagTree() {
		StringBuilder sb = new StringBuilder();
		toTagTree(sb, 0);
		return sb.toString();
	}

	private void toTagTree(StringBuilder sb, int indent) {
		char[] indentString = new char[indent * 2];
		Arrays.fill(indentString, ' ');
		sb.append(indentString).append("<").append(tag);
		Iterator<Map.Entry<String, String>> iter = attributes.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			sb.append(" ").append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
		}

		if (children == null)
			sb.append("/>");
		else {
			sb.append(">");
			boolean lfInserted = false;
			Iterator<Element> childIter = children.iterator();
			while (childIter.hasNext()) {
				Element child = childIter.next();
				if (child.text == null) {
					if (!lfInserted) {
						sb.append(LINE_SEPARATOR);
						lfInserted = true;
					}
					child.toTagTree(sb, indent + 1);
					sb.append(LINE_SEPARATOR).append(indentString);
				} else {
					sb.append(child.text);
				}
			}
			sb.append("</").append(tag).append(">");
		}
	}

	public String toTagString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<").append(tag);
		Iterator<Map.Entry<String, String>> iter = attributes.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, String> entry = iter.next();
			sb.append(" ").append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
		}
		sb.append(">");
		return sb.toString();
	}

}