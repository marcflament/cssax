package org.yah.tools.cssax.selector;

public enum PseudoClass {
	FIRST_CHILD("first-child"),
	NTH_CHILD("nth-child"),
	FIRST_OF_TYPE("first-of-type"),
	NTH_OF_TYPE("nth-of-type"),
	LAST_CHILD("last-child"),
	ROOT("root");

	private final String tokenName;

	private PseudoClass(String tokenName) {
		this.tokenName = tokenName;
	}

	public String getTokenName() {
		return tokenName;
	}

	public static PseudoClass fromToken(String token) {
		if (token == null)
			throw new IllegalArgumentException("token is null");
		String lowerToken = token.toLowerCase();
		for (PseudoClass pseudoClass : PseudoClass.values()) {
			if (pseudoClass.tokenName.equals(lowerToken))
				return pseudoClass;
		}
		return null;
	}

}
