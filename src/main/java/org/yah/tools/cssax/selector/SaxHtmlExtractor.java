/**
 * 
 */
package org.yah.tools.cssax.selector;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.ccil.cowan.tagsoup.AutoDetector;
import org.ccil.cowan.tagsoup.Parser;
import org.ccil.cowan.tagsoup.Schema;
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

/**
 * The service used to extract content from an html stream using some
 * {@link SelectorBinding}.
 * 
 * @author Yah
 */
public class SaxHtmlExtractor {

	private final SAXParserFactory saxParserFactory;

	private Schema htmlSchema;

	private final Charset encoding;

	public SaxHtmlExtractor(String charsetName) {
		this(Charset.forName(charsetName));
	}

	public SaxHtmlExtractor(Charset charset) {
		super();
		this.encoding = charset == null ? Charset.defaultCharset() : charset;
		saxParserFactory = SAXParserFactory.newInstance(SAXFactoryImpl.class.getName(), getClass().getClassLoader());
		saxParserFactory.setNamespaceAware(false);
	}

	public String getEncoding() {
		return encoding.name();
	}

	/**
	 * Allow to use a {@link Schema} to allow malformed HTML.
	 * @param htmlSchema
	 * @see https://hackage.haskell.org/package/tagsoup
	 */
	public void setHtmlSchema(Schema htmlSchema) {
		this.htmlSchema = htmlSchema;
	}

	private SAXParser createSaxParser() throws HtmlParsingException {
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.setProperty(Parser.autoDetectorProperty, new AutoDetector() {
				@Override
				public Reader autoDetectingReader(InputStream i) {
					return new InputStreamReader(i, encoding);
				}
			});
			if (htmlSchema != null)
				saxParser.setProperty(Parser.schemaProperty, htmlSchema);

			return saxParser;
		} catch (Exception e) {
			throw new HtmlParsingException("Error creating sax parser", e);
		}
	}

	public void parseHtml(InputStream htmlStream, SelectorBinding... bindings) throws HtmlParsingException {
		if (bindings.length == 0)
			throw new IllegalArgumentException("at least one binding is required");
		HtmlHandler handler = new HtmlHandler(bindings);
		SAXParser saxParser = createSaxParser();
		try {
			saxParser.parse(htmlStream, handler);
		} catch (SAXException e) {
			if (e.getCause() instanceof HtmlParsingException)
				throw (HtmlParsingException) e.getCause();
			throw new HtmlParsingException("Error parsing html stream", e);
		} catch (IOException e) {
			throw new HtmlParsingException("Error reading html stream", e);
		}
	}

	enum Selection {
		MATCHING,
		MATCHED,
		SKIP
	}

	private class HtmlHandler extends DefaultHandler2 {

		private ParsingContext parsingContext;

		private Locator locator;

		private int skippedDepth;

		public HtmlHandler(SelectorBinding[] bindings) {
			super();
			this.parsingContext = new ParsingContext(bindings);
		}

		@Override
		public void setDocumentLocator(Locator locator) {
			this.locator = locator;
		}

		@Override
		public void startDocument() throws SAXException {
		}

		@Override
		public void endDocument() throws SAXException {
			try {
				parsingContext.validate();
			} catch (HtmlParsingException e) {
				throw new SAXException(e);
			}
		}

		private int[] getLocation() {
			return locator == null ? null : new int[] { locator.getLineNumber(), locator.getColumnNumber() };
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {
			int[] location = getLocation();
			if (parsingContext.skipped())
				skippedDepth++;
			else {
				Element element = parsingContext.element;
				if (element == null)
					element = new Element(localName, attributes, location);
				else {
					Element newElement = element.addChildTag(localName, attributes, location);
					if (parsingContext.matched())
						element.addChild(newElement);
					element = newElement;
				}
				parsingContext.start(element);
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (skippedDepth > 0)
				skippedDepth--;
			else {
				Element matchedElement = parsingContext.element;
				try {
					parsingContext.end();
				} catch (HtmlParsingException e) {
					throw new SAXException(e);
				}

				if (!parsingContext.matched())
					matchedElement.cleanup();
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if (parsingContext.matched()) {
				String text = new String(ch, start, length);
				parsingContext.element.addText(text, getLocation());
			}
		}

	}

}
