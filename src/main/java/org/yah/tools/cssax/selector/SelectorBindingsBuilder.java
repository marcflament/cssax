/**
 * 
 */
package org.yah.tools.cssax.selector;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Yah
 * 
 */
public class SelectorBindingsBuilder {

	private final List<SelectorBinding> bindings = new LinkedList<SelectorBinding>();

	public static SelectorBindingsBuilder startBinding(String selector, SelectorListener listener)
			throws SelectorParsingException {
		SelectorBindingsBuilder res = new SelectorBindingsBuilder();
		return res.addBinding(selector, listener);
	}

	public static SelectorBindingsBuilder startBinding(SelectorBinding selectorBinding) {
		SelectorBindingsBuilder res = new SelectorBindingsBuilder();
		return res.addBinding(selectorBinding);
	}

	/**
	 * 
	 */
	public SelectorBindingsBuilder() {
		super();
	}

	public SelectorBindingsBuilder addBinding(String selector, SelectorListener listener)
			throws SelectorParsingException {
		bindings.add(new SelectorBinding(selector, listener));
		return this;
	}

	public SelectorBindingsBuilder addBinding(SelectorBinding selectorBinding) {
		bindings.add(selectorBinding);
		return this;
	}

	public SelectorBinding[] build() {
		return bindings.toArray(new SelectorBinding[bindings.size()]);
	}
}
