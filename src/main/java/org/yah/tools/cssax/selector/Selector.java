package org.yah.tools.cssax.selector;

import static org.yah.tools.cssax.selector.SelectorParser.*;


/**
 * A single CSS selector.
 * Use the ANTLR grammar from Jim Idle, Temporal Wave LLC. to parse CSS.
 * @author Yah
 *
 */
public class Selector {

	private final SelectorPart[] selectorParts;

	private final int[] specificity;

	private final String definition;

	Selector(SelectorPart[] selectorParts, int[] specificity) {
		super();
		this.selectorParts = selectorParts;
		this.specificity = specificity;
		this.definition = createDefinition();
	}

	public SelectorPart[] getSelectorParts() {
		return selectorParts;
	}

	private String createDefinition() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < selectorParts.length; i++) {
			if (i > 0) {
				switch (selectorParts[i].getCombinator()) {
				case DESCENDANT:
					sb.append(' ');
					break;
				case CHILD:
					sb.append('>');
					break;
				case ADJACENT_SIBLING:
					sb.append('+');
					break;
				case GENERAL_SIBLING:
					sb.append('~');
					break;
				default:
					throw new IllegalStateException("Unknown combinator "
							+ tokenNames[selectorParts[i].getCombinator()]);
				}
			}
			sb.append(selectorParts[i]);
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return definition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Selector other = (Selector) obj;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		return true;
	}

	public int[] getSpecificity() {
		return specificity;
	}

}