/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

import org.yah.tools.cssax.selector.Element;

/**
 * @author Yah
 *
 */
public class UniversalPredicate implements ElementPredicate {

	/**
	 * 
	 */
	public UniversalPredicate() {
		super();
	}

	@Override
	public boolean evaluate(Element element) {
		return true;
	}

	@Override
	public String toString() {
		return "*";
	}
}
