/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

import org.yah.tools.cssax.selector.Element;

/**
 * @author Yah
 * 
 */
public class CssClassPredicate implements ElementPredicate {

	private final String cssClass;

	public CssClassPredicate(String cssClass) {
		super();
		this.cssClass = cssClass;
	}

	@Override
	public boolean evaluate(Element element) {
		String elementClasses = element.getAttribute("class");
		if (elementClasses == null)
			return false;
		String[] split = elementClasses.split("\\s+");
		for (int i = 0; i < split.length; i++) {
			if (cssClass.equals(split[i]))
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "." + cssClass;
	}

}
