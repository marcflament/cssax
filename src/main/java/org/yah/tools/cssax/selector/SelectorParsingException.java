/**
 * 
 */
package org.yah.tools.cssax.selector;

/**
 * @author Yah
 *
 */
public class SelectorParsingException extends HtmlParsingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public SelectorParsingException() {
		super();
	}

	/**
	 * @param message
	 */
	public SelectorParsingException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SelectorParsingException(String message, Throwable cause) {
		super(message, cause);
	}

}
