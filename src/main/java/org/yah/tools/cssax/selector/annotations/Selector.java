/**
 * 
 */
package org.yah.tools.cssax.selector.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Yah
 * 
 */
@Target({ METHOD })
@Retention(RUNTIME)
public @interface Selector {
	/**
	 * The CSS selector definition
	 */
	String value();

	boolean expected() default false;
}
