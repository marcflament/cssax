/**
 * 
 */
package org.yah.tools.cssax.selector;

/**
 * @author Yah
 *
 */
public class ElementHandlerException extends HtmlParsingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ElementHandlerException() {
		super();
	}

	/**
	 * @param message
	 */
	public ElementHandlerException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ElementHandlerException(String message, Throwable cause) {
		super(message, cause);
	}

}
