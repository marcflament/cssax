/**
 * 
 */
package org.yah.tools.cssax.selector.predicates;

import org.yah.tools.cssax.selector.Element;


/**
 * @author Yah
 *
 */
public class TypePredicate implements ElementPredicate {
	
	private final String type;


	public TypePredicate(String type) {
		super();
		this.type = type;
	}


	@Override
	public boolean evaluate(Element element) {
		return type.equals(element.getTag());
	}
	
	@Override
	public String toString() {
		return type;
	}
}
