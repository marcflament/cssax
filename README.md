# cssax
Parse and extract HTML content using CSS selector in Java

Build and install :
	- Requires maven 3.
	- Install using : maven install

Maven dependency :
Include this in dependencies of your pom.xml
...
<dependency>
	<groupId>org.yah.tools</groupId>
	<artifactId>cssax</artifactId>
	<version>1.0.0</version>
</dependency>
...

Usage : 
```java

public static class ExampleExtractor {
	@Selector(".selected")
	public void extractByClass(Element e) {
		System.out.println("extractByClass : " + e);
	}

	// will be called before .selected selector since # is more specific
	@Selector("#second")
	public void extractById(Element e) {
		System.out.println("extractById : " + e);
	}

	@Selector("li")
	public void extractAllLi(Element e) {
		System.out.println("extractAllLi : " + e);
	}

	@Selector("ul")
	public void extractInner(Element e) {
		Element li = e.findFirst(new Selectors("#first"));
		System.out.println("extractInner : " + li + " extracted from " + e);
	}
	
}

public static void main(String[] args) throws Exception {
	// create the HTML extractor
	HtmlExtractor htmlExtractor = new HtmlExtractor(Charset.forName("UTF-8"));

	// extending a single selector binding
	InputStream htmlStream = Example.class.getResourceAsStream("/example.html");
	try {
		AnnotationSelectorBindingsFactory factory = new AnnotationSelectorBindingsFactory();
		SelectorBinding[] bindings = factory.createBindings(new ExampleExtractor());
		htmlExtractor.parseHtml(htmlStream, bindings);
	} finally {
		IOUtils.closeQuietly(htmlStream);
	}
}

```